//
//  PostCell.swift
//  Viper Test
//
//  Created by Михаил Полев on 03/01/2019.
//  Copyright © 2019 Михаил Полев. All rights reserved.
//

import AsyncDisplayKit
import UIKit

final class PostCell: ASCellNode {
    let text = ASTextNode()
    let desc = ASTextNode()
    
    override init() {
        super.init()
    }
    
    func initWithData(post: PostModel) {
        text.attributedText = NSAttributedString(
            string: post.title,
            attributes: [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15),
                NSAttributedString.Key.foregroundColor: UIColor.darkText,
                NSAttributedString.Key.kern: -0.3
            ])
        
        desc.attributedText = NSAttributedString(
            string: post.description,
            attributes: [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 11),
                NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.kern: -0.3
            ])
        
        addSubnode(text)
        addSubnode(desc)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let Layout =  ASStackLayoutSpec(
            direction: .vertical,
            spacing: 8,
            justifyContent: .center,
            alignItems: .start,
            children: [ text, desc ])
    
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10.0, left: 15.0, bottom: 10.0, right: 15.0), child: Layout)
    }
}
