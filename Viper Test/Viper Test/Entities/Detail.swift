//
//  Detail.swift
//  Viper Test
//
//  Created by Михаил Полев on 05/01/2019.
//  Copyright © 2019 Михаил Полев. All rights reserved.
//

import Foundation
import ObjectMapper

struct DetailModel {
    var id: Int = 0
    var title: String = ""
    var text: String = ""
    var image: String = ""
    var author: String = ""
    var date: String = ""
}

extension DetailModel: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        text <- map["text"]
        image <- map["image"]
        author <- map["author"]
        date <- map["date"]
    }
}
