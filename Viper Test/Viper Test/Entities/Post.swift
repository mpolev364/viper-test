//
//  Post.swift
//  Viper Test
//
//  Created by Михаил Полев on 03/01/2019.
//  Copyright © 2019 Михаил Полев. All rights reserved.
//

import Foundation
import ObjectMapper

struct PostModel {
    var id: Int = 0
    var title: String = ""
    var description: String = ""
}

extension PostModel: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
    }
}
